#+--------------------------------------------------------------------------------------------------------------+
#|  EXT_CSD
#+--------------------------------------------------------------------------------------------------------------+
# Supported Command Sets ([504],R)
S_CMD_SET = 0x1
!	
# HPI features ([503],R)
HPI_FEATURES = 0x1
!	bit[0] HPI_SUPPORT: 1 (HPI mechanism supported (default)) 
!	bit[1] HPI_IMPLEMENTATION: 0 (HPI mechanism implementation based on CMD13) 
# Background operations support ([502],R)
BKOPS_SUPPORT = 0x1
!	bit[0] SUPPORTED: 1 (Background operations are supported. 
!	The fields BKOPS_STATUS, BKOPS_EN, BKOPS_START and URGENT_BKOPS are supported. (default)) 
# Max packed read commands ([501],R)
MAX_PACKED_READS = 0x3F
!	
# Max packed write commands ([500],R)
MAX_PACKED_WRITES = 0x3F
!	
# Data Tag Support ([499],R)
DATA_TAG_SUPPORT = 0x1
!	bit[0] SYSTEM_DATA_TAG_SUPPORT: 1 (System Data Tag mechanism supported) 
# Tag Unit Size ([498],R)
TAG_UNIT_SIZE = 0x4
!	8 KiB
# Tag Resources Size ([497],R)
TAG_RES_SIZE = 0x0
!	0 B
# Context management capabilities ([496],R)
CONTEXT_CAPABILITIES = 0x5
!	bit[3:0] MAX_CONTEXT_ID: 5 
!	bit[6:4] LARGE_UNIT_MAX_MULTIPLIER_M1: 0 
# Large Unit size ([495],R)
LARGE_UNIT_SIZE_M1 = 0x7
!	8 MiB
# Extended partitions attribute support ([494],R)
EXT_SUPPORT = 0x3
!	bit[0] System code: 1 
!	bit[1] Non-persistent: 1 
# Supported modes ([493],R)
SUPPORTED_MODES = 0x1
!	bit[0] FFU: 1 
!	bit[1] VSM: 0 
# FFU features ([492],R)
FFU_FEATURES = 0x0
!	bit[0] SUPPORTED_MODE_OPERATION_CODES: 0 
# Operation codes timeout ([491],R)
OPERATION_CODE_TIMEOUT = 0x0
!	1us
# FFU Argument ([490:487],R)
FFU_ARG = 0xC7810000
!	
# Barrier support ([486],R)
BARRIER_SUPPORT = 0x0
!	
# CMD Queuing Support ([308],R)
CMDQ_SUPPORT = 0x0
!	
# CMD Queuing Depth ([307],R)
CMDQ_DEPTH = 0x0
!	
# Number of FW sectors correctly programmed ([305:302],R)
NUMBER_OF_FW_SECTORS_CORRECTLY_PROGRAMMED = 0x0
!	0
# Vendor proprietary health report ([301:270],R)
VENDOR_PROPRIETARY_HEALTH_REPORT = 0000000000000000000000000000000000000000000000000000000000000000
!	
# Device life time estimation type B ([269],R)
DEVICE_LIFE_TIME_EST_TYP_B = 0x1
!	0% - 10% device life time used
# Device life time estimation type A ([268],R)
DEVICE_LIFE_TIME_EST_TYP_A = 0x1
!	0% - 10% device life time used
# Pre EOL information ([267],R)
PRE_EOL_INFO = 0x1
!	Normal
# Optimal read size ([266],R)
OPTIMAL_READ_SIZE = 0x0
!	0 B
# Optimal write size ([265],R)
OPTIMAL_WRITE_SIZE = 0x10
!	64 KiB
# Optimal trim unit size ([264],R)
OPTIMAL_TRIM_UNIT_SIZE = 0x1
!	4 KiB
# Device version ([263:262],R)
DEVICE_VERSION = 0x0
!	0
# Firmware version ([261:254],R)
FIRMWARE_VERSION = 0600000000000000
!	6
# Power class for 200MHz, DDR at VCC = 3.6V ([253],R)
PWR_CL_DDR_200_360 = 0x0
!	100 mA-200 mA
# Cache size ([252:249],R)
CACHE_SIZE = 0x10000
!	64 MiB
# Generic CMD6 timeout ([248],R)
GENERIC_CMD6_TIME = 0xA
!	100ms
# Power off notification(long) timeout ([247],R)
POWER_OFF_LONG_TIME = 0x3C
!	600ms
# Background operations status ([246],R)
BKOPS_STATUS = 0x0
!	bit[1:0] OUTSTANDING: 0 (No operations required) 
# Number of correctly programmedsectors ([245:242],R)
CORRECTLY_PRG_SECTORS_NUM = 0x0
!	0
# 1st initialization time after partitioning ([241],R)
INI_TIMEOUT_AP = 0x1E
!	3000ms
# Cache Flushing Policy ([240],R)
CACHE_FLUSH_POLICY = 0x0
!	bit[0] FIFO: 0 (Device flushing policy is not provided by the device) 
# Power class for 52MHz, DDR at VCC = 3.6V ([239],R)
PWR_CL_DDR_52_360 = 0x0
!	100 mA-200 mA
# Power class for 52MHz, DDR at VCC = 1.95V ([238],R)
PWR_CL_DDR_52_195 = 0x0
!	65 mA-130 mA
# Power class for 200MHz at VCCQ=1.95V, VCC = 3.6V ([237],R)
PWR_CL_200_195 = 0x0
!	65 mA-130 mA
# Power class for 200MHz, at VCCQ=1.3V, VCC = 3.6V ([236],R)
PWR_CL_200_130 = 0x0
!	65 mA-130 mA
# Minimum Write Performance for 8bit at 52MHz in DDR mode ([235],R)
MIN_PERF_DDR_W_8_52 = 0x0
!	For Devices not reaching the 4.8MB/s value
# Minimum Read Performance for 8bit at 52MHz in DDR mode ([234],R)
MIN_PERF_DDR_R_8_52 = 0x0
!	For Devices not reaching the 4.8MB/s value
# TRIM Multiplier ([232],R)
TRIM_MULT = 0x2
!	600ms
# Secure Feature support ([231],R)
SEC_FEATURE_SUPPORT = 0x55
!	bit[6] SEC_SANITIZE: 1 (Device does not support the sanitize operation) 
!	bit[4] SEC_GB_CL_EN(R): 1 (Device supports the secure and insecure trim operations. This bit being set means that argument bits 15 and 0 are supported with CMD38) 
!	bit[2] SEC_BD_BLK_EN(R): 1 (Device supports the automatic erase operation on retired defective portions of the array. This bit being set enables the host to set SEC_BAD_BLK_MGMNT (extcsd_raw[134]).) 
!	bit[0] SECURE_ER_EN(R): 1 (Secure purge operations are supported. This bit being set allows the host to set bit 31 of the argument for the ERASE (CMD38) Command) 
# Secure Erase Multiplier ([230],R)
SEC_ERASE_MULT = 0x1B
!	8100ms
# Secure TRIM Multiplier ([229],R)
SEC_TRIM_MULT = 0x11
!	5100ms
# Boot information ([228],R)
BOOT_INFO = 0x7
!	bit[0] ALT_BOOT_MODE: 1 (Device supports alternative boot method. Device must show �1� since this is mandatory in v4.4 standard) 
!	bit[1] DDR_BOOT_MODE: 1 (Device supports dual data rate during boot) 
!	bit[2] HS_BOOT_MODE: 1 (Device supports high speed timing during boot) 
# Boot partition size ([226],R)
BOOT_SIZE_MULT = 0x20
!	4 MiB
# Access size ([225],R)
ACC_SIZE = 0x7
!	SUPER_PAGE_SIZE = 3.50 KiB
# High-capacity erase unit size ([224],R)
HC_ERASE_GRP_SIZE = 0x1
!	512 KiB
# High-capacity erase timeout ([223],R)
ERASE_TIMEOUT_MULT = 0x1
!	300ms
# Reliable write sector count ([222],R)
REL_WR_SEC_C = 0x1
!	1
# High-capacity write protect group size ([221],R)
HC_WP_GRP_SIZE = 0x10
!	16
# Sleep current (VCC) ([220],R)
S_C_VCC = 0x7
!	128uA
# Sleep current (VCCQ) ([219],R)
S_C_VCCQ = 0x7
!	128uA
# Production state awareness timeout ([218],R)
PRODUCTION_STATE_AWARENESS_TIMEOUT = 0x0
!	100us
# Sleep/awake timeout ([217],R)
S_A_TIMEOUT = 0x11
!	131072ns
# Sleep Notification Timout1 ([216],R)
SLEEP_NOTIFICATION_TIME = 0x7
!	1280us
# Sector Count ([215:212],R)
SEC_COUNT = 0x1D1F000
!	14.56 GiB
# Secure Write Protect Information ([211],R)
SECURE_WP_INFO = 0x0
!	bit[1] SECURE_WP_EN_STATUS(R): 0 (Legacy Write Protection mode.) 
!	bit[0] SECURE_WP_SUPPORT(R): 0 (Secure Write Protection is NOT supported by this device) 
# Minimum Write Performance for 8bit at 52 MHz ([210],R)
MIN_PERF_W_8_52 = 0x0
!	For Devices not reaching the 2.4MB/s value
# Minimum Read Performance for 8bit at 52 MHz ([209],R)
MIN_PERF_R_8_52 = 0x0
!	For Devices not reaching the 2.4MB/s value
# Minimum Write Performance for 8bit at 26 MHz, for 4bit at 52MHz ([208],R)
MIN_PERF_W_8_26_4_52 = 0x0
!	For Devices not reaching the 2.4MB/s value
# Minimum Read Performance for 8bit at 26 MHz, for 4bit at 52MHz ([207],R)
MIN_PERF_R_8_26_4_52 = 0x0
!	For Devices not reaching the 2.4MB/s value
# Minimum Write Performance for 4bit at 26 MHz ([206],R)
MIN_PERF_W_4_26 = 0x0
!	For Devices not reaching the 2.4MB/s value
# Minimum Read Performance for 4bit at 26 MHz ([205],R)
MIN_PERF_R_4_26 = 0x0
!	For Devices not reaching the 2.4MB/s value
# Power class for 26 MHz at 3.6 V ([203],R)
PWR_CL_26_360 = 0x0
!	100 mA-200 mA
# Power class for 52 MHz at 3.6 V ([202],R)
PWR_CL_52_360 = 0x0
!	100 mA-200 mA
# Power class for 26 MHz at 1.95 V ([201],R)
PWR_CL_26_195 = 0x0
!	65 mA-130 mA
# Power class for 52 MHz at 1.95 V ([200],R)
PWR_CL_52_195 = 0x0
!	65 mA-130 mA
# Partition switching timing ([199],R)
PARTITION_SWITCH_TIME = 0x1
!	100ms
# Out-of-interrupt busy timing ([198],R)
OUT_OF_INTERRUPT_TIME = 0x5
!	50ms
# I/O Driver Strength ([197],R)
DRIVER_STRENGTH = 0x1F
!	bit[0] Type 0: 1 (supported (mandatory)) 
!	bit[1] Type 1: 1 (supported)) 
!	bit[2] Type 2: 1 (supported) 
!	bit[3] Type 3: 1 (supported) 
!	bit[4] Type 4: 1 (supported) 
# Device type ([196],R)
DEVICE_TYPE = 0x57
!	High-Speed e�MMC at 26 MHz - at rated device voltage(s)
!	High-Speed e�MMC at 52 MHz - at rated device voltage(s)
!	High-Speed Dual Data Rate e�MMC at 52 MHz - 1.8 V or 3 V I/O
!	HS200 Single Data Rate e�MMC at 200 MHz - 1.8 V I/O
!	HS400 Dual Data Rate e�MMC at 200 MHz - 1.8 V I/O
# CSD STRUCTURE ([194],R)
CSD_STRUCTURE = 0x2
!	1.2(Version 4.1-4.2-4.3-4.41-4.5-4.51-5.0-5.01-5.1)
# Extended CSD revision ([192],R)
EXT_CSD_REV = 0x7
!	Revision 1.7 (for MMC v5.0, v5.01)
# Command set ([191],R/W/E_P)
CMD_SET = 0x0
!	0
# Command set revision ([189],R)
CMD_SET_REV = 0x0
!	0
# Power class ([187],R/W/E_P)
POWER_CLASS = 0x0
!	bit[3:0] Device power class code: 0 (100 mA-200 mA) 
# High-speed interface timing ([185],R/W/E_P)
HS_TIMING = 0x1
!	High Speed
# Strobe Support ([184],R)
STROBE_SUPPORT = 0x1
!	1
# Bus width mode ([183],W/E_P)
BUS_WIDTH = 0x0
!	1 bit data bus
# Erased memory content ([181],R)
ERASED_MEM_CONT = 0x0
!	Erased memory range shall be �0�
# Partition configuration ([179],R/W/E & R/W/E_P)
PARTITION_CONFIG = 0x0
!	bit[6] BOOT_ACK (R/W/E): 0 (No boot acknowledge sent (default)) 
!	bit[5:3] BOOT_PARTITION_ENABLE (R/W/E): 0 (Device not boot enabled (default)) 
!	bit[2:0] PARTITION_ACCESS (before BOOT_PARTITION_ACCESS, R/W/E_P): 0 (No access to boot partition (default)) 
# Boot config protection ([178],R/W & R/W/C_P)
BOOT_CONFIG_PROT = 0x0
!	bit[4] PERM_BOOT_CONFIG_PROT (R/W): 0 (PERM_BOOT_CONFIG_PROT is not enabled (default)) 
!	bit[0] PWR_BOOT_CONFIG_PROT (R/W/C_P): 0 (PWR_BOOT_CONFIG_PROT is not enabled (default)) 
# Boot bus Conditions ([177],R/W/E)
BOOT_BUS_CONDITIONS = 0x0
!	bit[4:3] BOOT_MODE (nonvolatile): 0 (Use single data rate + backward compatible timings in boot operation (default)) 
!	bit[2] RESET_BOOT_BUS_CONDITIONS (nonvolatile): 0 (Reset bus width to x1, single data rate and backward compatible timings after boot operation (default)) 
!	bit[1:0] BOOT_BUS_WIDTH (nonvolatile): 0 (x1 (sdr) or x4 (ddr) bus width in boot operation mode (default)) 
# High-density erase group definition ([175],R/W/E_P)
ERASE_GROUP_DEF = 0x1
!	bit[0] ENABLE: 1 (Use high-capacity erase unit size, high capacity erase timeout, and high-capacity write protect group size definition.) 
# Boot write protection status registers ([174],R)
BOOT_WP_STATUS = 0x0
!	bit[3:2] B_AREA_2_WP (R): 0 (Boot Area 2 is not protected) 
!	bit[1:0] B_AREA_1_WP (R): 0 (Boot Area 1 is not protected) 
# Boot area write protection register ([173],R/W & R/W/C_P)
BOOT_WP = 0x0
!	bit[7] B_SEC_WP_SEL (R/W/C_P): 0 (B_PERM_WP_EN(bit2) and B_PWR_WP_EN (bit 0) apply to both boot partitions and B_PERM_WP_SEC_SEL (bit 3) and B_PWR_WP_SEC_SEL (bit 1) have no impact) 
!	bit[6] B_PWR_WP_DIS (R/W/C_P): 0 (Master is permitted to set B_PWR_WP_EN(bit 0)) 
!	bit[4] B_PERM_WP_DIS (R/W): 0 (Master is permitted to set B_PERM_WP_EN(bit 2)) 
!	bit[3] B_PERM_WP_SEC_SEL(R/W/C_P): 0 (B_PERM_WP_EN(Bit 2) applies to boot Area1 only, if B_SEC_WP_SEL (bit 7 is set)) 
!	bit[2] B_PERM_WP_EN (R/W): 0 (Boot region is not permanently write protected.) 
!	bit[1] B_PWR_WP_SEC_SEL(R/W/C_P): 0 (B_PWR_WP_EN(Bit 0) applies to boot Area1 only, if B_SEC_WP_SEL (bit 7 is set)) 
!	bit[0] B_PWR_WP_EN (R/W/C_P): 0 (Boot region is not power-on write protected.) 
# User area write protection register ([171],R/W,R/W/C_P & R/W/E_P)
USER_WP = 0x0
!	bit[7] PERM_PSWD_DIS (R/W): 0 (Password protection features are enabled.) 
!	bit[6] CD_PERM_WP_DIS (R/W): 0 (Host is permitted to set PERM_WRITE_PROTECT (CSD[13]).) 
!	bit[4] US_PERM_WP_DIS (R/W): 0 (Permanent write protection can be applied to write protection groups.) 
!	bit[3] US_PWR_WP_DIS (R/W/C_P): 0 (Power-on write protection can be applied to write protection groups.) 
!	bit[2] US_PERM_WP_EN (R/W/E_P): 0 (Permanent write protection is not applied when CMD28 is issued.) 
!	bit[0] US_PWR_WP_EN (R/W/E_P): 0 (Power-on write protection is not applied when CMD28 is issued.) 
# FW configuration ([169],R/W)
FW_CONFIG = 0x0
!	bit[0] Update_Disable: 0 (FW updates enabled.) 
# RPMB Size ([168],R)
RPMB_SIZE_MULT = 0x20
!	4 MiB
# Write reliability setting register ([167],R/W)
WR_REL_SET = 0x1F
!	bit[4] WR_DATA_REL_4: 1 (In general purpose partition 4, the device protects previously written data if power failure occurs.) 
!	bit[3] WR_DATA_REL_3: 1 (In general purpose partition 3, the device protects previously written data if power failure occurs.) 
!	bit[2] WR_DATA_REL_2: 1 (In general purpose partition 2, the device protects previously written data if power failure occurs. ) 
!	bit[1] WR_DATA_REL_1: 1 (In general purpose partition 1, the device protects previously written data if power failure occurs.) 
!	bit[0] WR_DATA_REL_USR: 1 (In the main user area, the device protects previously written data if power failure occurs.) 
# Write reliability parameter register ([166],R)
WR_REL_PARAM = 0x14
!	bit[4] EN_RPMB_REL_WR (R): 1 (RPMB transfer size is either 256B (single 512B frame), 512B (two 512B frame), or 8KB (Thirty two 512B frames).) 
!	bit[2] EN_REL_WR (R): 1 (The device supports the enhanced definition of reliable write) 
!	bit[0] HS_CTRL_REL (R): 0 (obsolete) 
# Start Sanitize operation ([165],W/E_P)
SANITIZE_START = 0x0
!	0
# Manually start background operations ([164],W/E_P)
BKOPS_START = 0x0
!	0
# Enable background operations handshake ([163],R/W & R/W/E)
BKOPS_EN = 0x0
!	bit[1] AUTO_EN (R/W/E) default value is vendor specific.: 0 (Device shall not perform background operations while not servicing the host.) 
!	bit[0] MANUAL_EN (R/W): 0 (Host does not support background operations handling and is not expected to write to BKOPS_START field.) 
# H/W reset function ([162],R/W)
RST_n_FUNCTION = 0x1
!	bit[1:0] RST_n_ENABLE (Readable and Writable once): 1 (RST_n signal is permanently enabled) 
# HPI management ([161],R/W/E_P)
HPI_MGMT = 0x0
!	bit[0] HPI_EN: 0 (HPI mechanism not activated by the host (default)) 
# Partitioning Support ([160],R)
PARTITIONING_SUPPORT = 0x7
!	bit[2] EXT_ATTRIBUTE_EN: 1 (Device can have extended partitions attribute) 
!	bit[1] ENH_ATTRIBUTE_EN: 1 (Device can have enhanced technological features in partitions and user data area) 
!	bit[0] PARTITIONING_EN: 1 (Device supports partitioning features) 
# Max Enhanced Area Size ([159:157],R)
MAX_ENH_SIZE_MULT = 0x3A3
!	3.27 GiB
# Partitions attribute ([156],R/W)
PARTITIONS_ATTRIBUTE = 0x0
!	bit[4] ENH_4: 0 (Default) 
!	bit[3] ENH_3: 0 (Default) 
!	bit[2] ENH_2: 0 (Default) 
!	bit[1] ENH_1: 0 (Default) 
!	bit[0] ENH_USR: 0 (Default) 
# Partitioning Setting ([155],R/W)
PARTITION_SETTING_COMPLETED = 0x0
!	bit[0] PARTITION_SETTING_COMPLETED: 0 
# General Purpose Partition Size ([154:143],R/W)
GP_SIZE_MULT = 000000000000000000000000
!	GP_SIZE_1: 0 B 
!	GP_SIZE_2: 0 B 
!	GP_SIZE_3: 0 B 
!	GP_SIZE_4: 0 B 
# Enhanced User Data Area Size ([142:140],R/W)
ENH_SIZE_MULT = 0x0
!	0 B 
# Enhanced User Data Start Address ([139:136],R/W)
ENH_START_ADDR = 0x0
!	0x00000000
# Bad Block Management mode ([134],R/W)
SEC_BAD_BLK_MGMNT = 0x0
!	bit[0] SEC_BAD_BLK (R/W): 0 ((Default) Feature disabled) 
# Production state awareness ([133],R/W/E)
PRODUCTION_STATE_AWARENESS = 0x0
!	NORMAL(Field)
# Package Case Temperature is controlled ([132],W/E_P)
TCASE_SUPPORT = 0x0
!	
# Periodic Wake-up ([131],R/W/E)
PERIODIC_WAKEUP = 0x0
!	bit[4:0] WAKEUP_PERIOD: 0 
!	bit[7:5] WAKEUP_UNIT: 0 (infinity (no wakeups)) 
# Program CID/CSD in DDR mode support ([130],R)
PROGRAM_CID_CSD_DDR_SUPPORT = 0x1
!	bit[0] PROGRAM_CID_CSD_DDR_SUPPORT (R): 1 (CMD26 and CMD27 are considered legal in both single data rate and dual data rate mode.) 
# Vendor Specific Fields ([127:64],<vendor specific>)
VENDOR_SPECIFIC_FIELD = 0f000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
!	
# Native sector size ([63],R)
NATIVE_SECTOR_SIZE = 0x0
!	Native sector size is 512B
# Sector size emulation ([62],R/W)
USE_NATIVE_SECTOR = 0x0
!	Device is emulating a 512B sector size or uses a native 512B sector size
# Sector size ([61],R)
DATA_SECTOR_SIZE = 0x0
!	Data sector size is 512B
# 1st initialization after disabling sectorsize emulation ([60],R)
INI_TIMEOUT_EMU = 0x0
!	0ms
# Class 6 commands control ([59],R/W/E_P)
CLASS_6_CTRL = 0x0
!	Write Protect (Default)
# Number of addressed group to be Released ([58],R)
DYNCAP_NEEDED = 0x0
!	
# Exception events control ([57:56],R/W/E_P)
EXCEPTION_EVENTS_CTRL = 0x0
!	
# Exception events status ([55:54],R)
EXCEPTION_EVENTS_STATUS = 0x0
!	
# Extended Partitions Attribute ([53:52],R/W)
EXT_PARTITIONS_ATTRIBUTE = 0x0
!	
# Context configuration ([51:37],R/W/E_P)
CONTEXT_CONF = 000000000000000000000000000000
!	bit[7:6] Reliability mode: 0 (MODE0 (normal)) 
!	bit[5:3] Large Unit multiplier: 0 
!	bit[2] Large Unit context: 0 (Context is not following Large Unit rules) 
!	bit[1:0] Activation and direction selection: 0 (Context is closed and is no longer active) 
# Packed command status ([36],R)
PACKED_COMMAND_STATUS = 0x0
!	
# Packed command failure index ([35],R)
PACKED_FAILURE_INDEX = 0x0
!	
# Power Off Notification ([34],R/W/E_P)
POWER_OFF_NOTIFICATION = 0x1
!	[POWERED_ON] Host shall notify before powering off the device, and keep power supplies alive and active until then
# Control to turn the Cache ON/OFF ([33],R/W/E_P)
CACHE_CTRL = 0x0
!	bit[0] CACHE_EN: 0 (Cache is OFF) 
# Flushing of the cache ([32],W/E_P)
FLUSH_CACHE = 0x0
!	bit[1] BARRIER: 0 (Reset value) 
!	bit[0] FLUSH: 0 (Reset value) 
# Control to turn the Barrier ON/OFF ([31],R/W)
BARRIER_CTRL = 0x0
!	bit[0] BARRIER_EN: 0 (Barrier feature is OFF) 
# Mode config ([30],R/W/E_P)
MODE_CONFIG = 0x0
!	Normal Mode
# Mode operation codes ([29],W/E_P)
MODE_OPERATION_CODES = 0x0
!	Reserved
# FFU status ([26],R)
FFU_STATUS = 0x0
!	Success
# Pre loading data size ([25:22],R/W/E_P)
PRE_LOADING_DATA_SIZE = 0x0
!	0 B
# Max pre loading data size ([21:18],R)
MAX_PRE_LOADING_DATA_SIZE = 0x0
!	0 B
# Product state awareness enablement ([17],R/W/E & R)
PRODUCT_STATE_AWARENESS_ENABLEMENT = 0x0
!	0
# Secure Removal Type ([16],R/W & R)
SECURE_REMOVAL_TYPE = 0x9
!	bit[5:4] Configure Secure Removal Type: 0 (information removed by an erase of the physical memory) 
!	bit[3:0] Supported Secure Removal Type: 9 
# Command Queue Mode Enable ([15],R/W/E_P)
CMDQ_MODE_EN = 0x0
!	bit[0] Command queuing enable: 0 (Command queuing is disabled. Host should clear the queue empty using CMD48 prior disabling the queue.) 
